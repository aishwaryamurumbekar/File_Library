#define OPEN_MAX 20		// maximum 20 files can be opened
#define OL 0 			// offset is set to zero in lseek
#define BUFSIZE 1024		// Maximum size of the buffer in single call of fillbuf

typedef struct FILES {
	int cntl;		// number of character left
	int totcnt;		// total charecters in buffer i.e. in fp->base 
	char *ptr;		// pointer to the next character
	char *base;		// location where data is stored temporarly
	int flag;		// to show the current mode
	int fd;			// file descripter
	int bufstatus;		// to check the buffer is empty or filled
}FILES;

typedef struct myfpos_t {	
	int cnt;
	char *pt;
} myfpos_t;			// this structure is used in myfgetpos and myfsetpos to store and use the position 

enum flags {		
	READ = 01,
	WRITE = 02,
	APPEND = 03,
	BUFFERED = 04,
	RP = 05,
	WP = 06,
	AP = 07,
	_EOF = 010,
	_ERR = 020
};				// flags are preset to set the mode of fopen

FILES *myfopen(char *name, char *mode);
int fillbuf(FILES *fp);
int myfread(void *buffer, int size, int count, FILES *fp);
int myfwrite(void *buffer, int size, int count, FILES *fp);
int myfseek(FILES *fp, int offset, int whence);
int myftell(FILES *fp);
int myfsetpos(FILES *fp, myfpos_t *pos);
int myfgetpos(FILES *fp, myfpos_t *pos);
int myfeof(FILES *fp);
int flushbuf(FILES *fp);
int myfclose(FILES *fp);
