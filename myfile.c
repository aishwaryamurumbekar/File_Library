#include<stdio.h>
#include<string.h>
#include<strings.h>
#include<errno.h>
#include<fcntl.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<stdlib.h>
#include<unistd.h>
#include"myfile.h"

FILES *fp;
/* slot[] is the array of file pointers. 
 * Each slot holds single file. 
 * Here first 3 elements of slot are initialised to input file(keyboard), output file(display), error file.
 */

FILES slot[OPEN_MAX] = {{0, 0, NULL, NULL, READ, 0}, {0, 0, NULL, NULL, WRITE, 1}, {0, 0, NULL, NULL, _ERR, 2}};

/* myfopen starts here.
 * File can open in 6 modes.
 * read(r), write(w), append(a), read-write(r+), write-read(w+), a+.
 */

FILES *myfopen(char *name, char *mode){
	int fd;
	if(strcmp(mode, "r") && strcmp(mode, "w") && strcmp(mode, "a") && strcmp(mode, "r+") && strcmp(mode, "w+") && strcmp(mode, "a+")){
		return NULL;	
	}
	for(fp = slot; fp < slot + OPEN_MAX; fp++){
		if((fp->flag != READ) && (fp->flag != WRITE) && (fp->flag != APPEND) && (fp->flag != RP) && (fp->flag != WP) && (fp->flag != AP))
			break; 			// slot found
	}
	if(fp >= slot + OPEN_MAX) 		// more than OPEN_MAX files cannot be opened
		return NULL;

	if(!strcmp(mode, "r")) {
		fd = open(name, O_RDONLY | O_CREAT, S_IRUSR | S_IWUSR); 
		fp->flag = READ;
	}
	else if(!strcmp(mode, "w")) {
		fd = creat(name, O_WRONLY | S_IRUSR | S_IWUSR);	// creat function truncate the file first	
		fp->flag = WRITE;
	}
	else if(!strcmp(mode, "a")) {	
		fd = open(name, O_WRONLY | S_IRUSR | S_IWUSR);
		lseek(fd, OL, SEEK_END);	// we can write only at the end of file in append mode
		fp->flag = APPEND;		// so pointer in the file is taken to the end of file
	}
	else if(!strcmp(mode, "r+")) {
		fd = open(name, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
		fp->flag = RP;
	}
	else if(!strcmp(mode, "w+")) {	
		fd = creat(name, S_IRUSR | S_IWUSR | O_RDWR);
		fp->flag = WP;
	}
	else if(!strcmp(mode, "a+")) {		// data can read from any part of file 
		fd = open(name, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);	
		fp->flag = AP;			// but data can be written at the end of file
	}
	printf("fp->fd = %d ", fd);
	if(fd == -1) {				//fopen failed
		printf("myfopen failed\n");
		return NULL;
	}
	fp->fd = fd;
	fp->cntl = 0;
	fp->ptr = NULL;
	fp->base = NULL;
	return fp;
}

/* The complete data from file is read and stored in temporary memory i.e. buffer. 
 * Using this fillbuf function all data gets filled in buffer. 
 * All changes are done in buffer. 
 * And then file gets updated using flushbuf while closing the file. 
 */
 
int fillbuf(FILES *fp){	
	int bufsize = BUFSIZE;
	fp->base = (char *)malloc(bufsize);
	fp->ptr = fp->base;
	if((fp->flag == READ) || (fp->flag == RP) || (fp->flag == AP)) {
		fp->totcnt = fp->cntl = read(fp->fd, fp->ptr, bufsize);
		lseek(fp->fd, 0, SEEK_SET);
	}
	else if((fp->flag == WRITE) || (fp->flag == WP)) {
		fp->totcnt = fp->cntl = 0;
		lseek(fp->fd, 0, SEEK_SET);
	}
	else if(fp->flag == APPEND) {
		fp->totcnt = fp->cntl = 0;
		lseek(fp->fd, 0, SEEK_END);
	}
	fp->bufstatus = BUFFERED;
	if(fp->cntl < 0) {
		if(fp->cntl == -1)
			fp->flag = _EOF;
		else
			fp->flag = _ERR;
		fp->cntl = 0;
		return EOF;
	}
	return 1;
}

/* myfread starts here.
 * myfread calls fillbuf to read data into buffer 
 * then copy the memory to buf according to given arguments
 */
 
int myfread(void *buf, int size, int count, FILES *fp) {
	char *data;
	unsigned int countl;			// char left to read
	unsigned int total;			// total no of char to read
	unsigned int nbytes, nrd = 0;
	data = (char *)buf;
	if((countl = total = size * count) == 0)
		return 0;
	if(fp->bufstatus != BUFFERED) {		// buffer is filled only once
		fillbuf(fp);
	}
	while(countl != 0) {
// buffer already exist, has char in it. i.e. fillbuf is already called and fp->cntl is set to some value.
		nbytes = (countl < fp->cntl) ? countl : fp->cntl;
		if(fp->cntl == 0) {
			perror("Pointer reaches end of the file\n");
			break;
		}
		memcpy(data, fp->ptr, nbytes);
		buf = data;
		countl -= nbytes;
		fp->cntl -= nbytes;
		fp->ptr += nbytes;
		data += nbytes;
		nrd = nrd + nbytes;	}
	return (nrd/size);
}

/* myfwrite starts here.
 * Data in buf is copied to buffer at perticular position
 */
int myfwrite(void *buf, int size, int count, FILES *fp) {
	char *data;
	unsigned int countl;			//char left to read
	unsigned int total;			//total no of char to read
	data = (char *)buf;
	if((countl = total = size * count) == 0)
		return 0;
	if(fp->bufstatus != BUFFERED) {
		fillbuf(fp);
	}
	if((fp->flag == APPEND) || (fp->flag == AP)) {
		fp->ptr = fp->ptr + fp->cntl;
		fp->cntl = 0;
		
	}
	memcpy(fp->ptr, data, countl);
	fp->ptr += countl;
	if(countl <= fp->cntl) {
		fp->cntl = fp->cntl - countl ;
	}
	else {					// fp->ptr is at the end of buffer
		fp->cntl = 0;
		fp->totcnt = (fp->ptr - fp->base);
	}
	return (countl / size);
}

/* myfseek starts here
 */
int myfseek(FILES *fp, int offset, int whence) {
	if(whence == SEEK_SET) {
		fp->ptr = fp->base + offset;
		fp->cntl = fp->totcnt - offset;
		if(offset >= 0) {
			return 0;	
		}
	}
	else if(whence == SEEK_CUR) {
		fp->ptr = fp->ptr + offset;
		fp->cntl -= offset;
		if(offset >= -(fp->ptr - fp->base)) {
			return 0;	
		}
	}
	else if(whence == SEEK_END) {
		fp->ptr = fp->base + fp->totcnt + offset;
		fp->cntl = - (offset);
		if(offset <= 0) {
			return 0;	
		}
	}
	return -1;
}

/* myftell starts here
 * returns current position 
 */
int myftell(FILES *fp) {
		return (fp->ptr - fp->base);
}

/* myfsetpos sets position in the buffer to pos
 */
int myfsetpos(FILES *fp, myfpos_t *pos) {
	fp->cntl = pos->cnt;
	fp->ptr = pos->pt;
	return 0;
}

/* myfgetpos stores the current position to pos
 */
int myfgetpos(FILES *fp, myfpos_t *pos) {
	pos->pt = fp->ptr;	
	pos->cnt = fp->cntl;
	return 0;
}

// It checks whether the fp->ptr reaches to the end or not  
int myfeof(FILES *fp) {
	if(fp->cntl == 0) {
		return 1;
	}
	return 0;
}

int flushbuf(FILES *fp) {
	int w;
	//printf("fp->base in write = %s\n", fp->base);
	while((fp->totcnt > 0) && (w != -1)) {
		w = write(fp->fd, fp->base, fp->totcnt);
		fp->totcnt -= w;
	}
	if(w == -1) {
		printf("Data is not writen in the file\n");
		return EOF;
	}
	else {
	printf("Data is written in file\n");
	}
	if(fp->cntl == 0) {
		free(fp->base);
		fp->base = fp->ptr = NULL;
		fp->cntl = 0;
	}
	return 1;
}

/* myfclose starts here 
 * It calls flushbuf to write buffer into the file
 * Then it closes the file
 */
int myfclose(FILES *fp) {
	flushbuf(fp);
	return close(fp->fd);	
}

