/* 
 * This testcode is to test the different cases of myfile library functions
 * Each comment bress contain a test 
 * So just remove comment symbol to run that perticular case 
 */
#include<stdio.h>
#include<string.h>
#include<strings.h>
#include<errno.h>
#include<fcntl.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<stdlib.h>
#include<unistd.h>
#include"myfile.h"


// test1c :Opening 3 files at a time for writing in w mode with different containts like char, int array, struct.

int main() {
	char str[40] = "abcdefghijklmnopqrstuvwxyz";
	int arr[10] = {1, 10, 100, 1000, 10000};
	typedef struct data {
		char name[16];
		long int mis;
		float age;
	}data;
	data d;
	d.name[16] = "Aishwarya", d.mis = 111608044, d.age = 19.5;
	int dwc, dwi, dws;
	FILES *fpc, *fpi, *fps;
	printf("TEST 1 :\n");
	fpc = myfopen("textfile", "w");
	fpi = myfopen("intfile", "w");
	fps = myfopen("structfile", "w");
	dwc = myfwrite(str, 26, 1, fpc);
	dwi = myfwrite(arr, sizeof(int), 4, fpi);
	dws = myfwrite(&d, sizeof(data), 1, fps);
	printf("\ndwc = %d, dwi = %d, dws = %d\n", dwc, dwi, dws);
	printf("close => text %d\n", myfclose(fpc));
	printf("close => int %d\n", myfclose(fpi));
	printf("close => struct %d\n", myfclose(fps));
	return 0;
}

// test2c :Opening 3 files at a time for reading and writing in w+ mode with different containts like char, int array, struct

/*int main() {
	char str[40] = "abcdefghijklmnopqrstuvwxyz", str1[40];
	int arr[10] = {1, 10, 100, 1000, 10000}, arr1[10], i;
	typedef struct data {
		char name[16];
		long int mis;
		float age;
	}data;
	data d, d1;
	d.mis = 111608044, d.age = 19.5;
	strcpy(d.name, "aishwarya");
	int dwc, dwi, dws, drc, dri, drs;
	FILES *fpc, *fpi, *fps;
	printf("TEST 2 :\n");
	fpc = myfopen("textfile", "w+");
	fpi = myfopen("intfile", "w+");
	fps = myfopen("structfile", "w+");
	dwc = myfwrite(str, 26, 1, fpc);
	dwi = myfwrite(arr, sizeof(int), 4, fpi);
	dws = myfwrite(&d, sizeof(data), 1, fps);
	myfseek(fpc, 0, SEEK_SET);
	myfseek(fpi, 0, SEEK_SET);
	myfseek(fps, 0, SEEK_SET);
	drc = myfread(str1, 26, 1, fpc);
	dri = myfread(arr1, sizeof(int), 4, fpi);
	drs = myfread(&d1, sizeof(data), 1, fps);
	printf("\ndwc = %d, dwi = %d, dws = %d\n", dwc, dwi, dws);
	printf("drc = %d, dri = %d, drs = %d\n", drc, dri, drs);
	printf("read data :\n");
	printf("textfile: %s\n", str1);
	printf("intfile: ");
	for(i = 0; i < 4; i++) {
		printf("%d ", arr1[i]);
	}
	printf("\nstructfile : name : %s MIS : %ld age : %.2f\n", d1.name, d1.mis, d1.age);
	myfclose(fpc);
	myfclose(fpi);
	myfclose(fps);
	return 0;
}*/

// test3c :r+ mode. use of all functions 

/*int main() {
	FILES *fp;
	myfpos_t pos;
	char str[40], str1[40] = "xxxxxxxx";
	printf("TEST 3 :\n");
	fp = myfopen("textfile", "r+");	
	myfread(str, 10, 2, fp);
	printf("read data : %s\n", str);
	myfgetpos(fp, &pos);
	printf("myftell after read = %d", myftell(fp));
	myfwrite(str1, 3, 1, fp);
	myfsetpos(fp, &pos);
	myfread(str, 4, 2, fp);
	printf("read data : %s\n", str);
	myfseek(fp, -5, SEEK_END);
	myfwrite(str1, 4, 1, fp);
	myfclose(fp);
	return 0;
}*/

// test4c :Read data from one file write it to another file

/*int main() {
	FILES *fp1, *fp2;
	char str1[40], str2[40];
	printf("TEST 4 :\n");
	fp1 = myfopen("textfile", "r");
	fp2 = myfopen("newfile", "w");
	myfread(str1, 6, 4, fp1);
	printf("read data : %s\n", str1);
	myfwrite(str1, 6, 4, fp2);
	myfseek(fp2, 0, SEEK_SET);
	myfread(str2, 6, 4, fp2);
	printf("data written in newfile : %s\n", str2);
	printf("read file close : ");
	myfclose(fp1);
	printf("new file close : ");
	myfclose(fp2);
	return 0;
}*/

// test5c : Append data in the file in a mode

/*int main() {
	FILES *fp;
	char str1[40] = "xxxxxxxxxxxx";
	printf("TEST 5 :\n");
	fp = myfopen("appendfile", "a"); 
	myfwrite(str1, 2, 4, fp);
	myfclose(fp);
	return 0;
}*/

// test6c : Append data in the file and read it in a+ mode

/*int main() {
	FILES *fp;
	char str1[40] = "xxxxxxxxxxxx", str[40];
	printf("TEST 6 :\n");
	fp = myfopen("appendfile", "a+"); 
	myfwrite(str1, 2, 4, fp);
	myfseek(fp, -25, SEEK_CUR);
	myfread(str, 20, 1, fp);
	printf("\nread data : %s\n", str);
	myfclose(fp);
	return 0;
}*/

