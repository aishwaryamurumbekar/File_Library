Project : File_Library

Name 	: Aishwarya Govind Murumbekar
MIS	: 111608044

This project is file library of f-functions in file handling.
In this project 9 file functions such that myfopen, myfread, 
myfwrite, myfseek, myftell, myfsetpos, myfgetpos, myfeof, myfclose are written
using the basic functions of file such that open, read, write, close, lseek.
Also there are 2 functions fillbuf, flushbuf used to fill or empty buffer 
respectively.
This project is similar to file library in stdio.h
so the syntax are almost same. So this file can be used in file handeling codes
    
Thank u Abhijit sir for giving us opportunity to explore ourself to vast 
knowlege and to work on our own project.

HOW TO RUN THE PROJECT :
	
>> make
>> ./project
